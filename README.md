# PHP multidimensional array search by value

Extract and get array values from multidimensional array using php


## How To Use

```sh
<?php 

use TyExtractNestedArray\ExtractNestedArray;

// use data array
$records = array(
    array(
        'id' => 2135,
        'first_name' => 'John',
        'last_name' => 'Doe',
    ),
    array(
        'id' => 3245,
        'first_name' => 'Sally',
        'last_name' => 'Smith',
    ),
    array(
        'id' => 5342,
        'first_name' => 'Jane',
        'last_name' => 'Jones',
        'address' => 'yok',
    ),
);

// define extract class
$findNested = new ExtractNestedArray();

// you search field
$field = 'first_name';

// call execute for extract from data
$result = $findNested->extractNested($records, $field);
print_r($result);
```

## Use Case & Examples

Some examples

src/Examples/Example.php

You data define

```sh

/* ------------------------------ you data ------------------------------ */
$records = array(
    array(
        'id' => 2135,
        'first_name' => 'John',
        'last_name' => 'Doe',
    ),
    array(
        'id' => 3245,
        'first_name' => 'Sally',
        'last_name' => 'Smith',
    ),
    array(
        'id' => 5342,
        'first_name' => 'Jane',
        'last_name' => 'Jones',
        'address' => 'yok',
    ),
    array(
        'id' => 5623,
        'first_name' => 'Peter',
        'last_name' => 'Doe',
        'age' => 2,
        'address' => [
            'id'=> 34,
            'city'=> 'ist',
            'services'=> [
                'prop'=> [
                    'fastDelivery',
                    'support',
                    'centerPoint'
                ],
            ],
        ],
        'cart'=> [
            'payment'=> 'creditCard',
            'total'=> 14.90,
            'items'=> [
                [
                    'id'=> 232,
                    'price'=> 10.00,
                    'quantity'=> 1,
                    'campaign'=> 1,
                    'product'=> [
                        'sku'=> 'aa',
                        'brand'=> 'apple'
                    ]
                ],
                [
                    'id'=> 343,
                    'price'=> 20.00,
                    'quantity'=> 1,
                    'product'=> [
                        'sku'=> 'bb',
                        'brand'=> 'bosch',
                        'category'=> [
                            'electronic',
                            'phone'
                        ]
                    ]
                ]
            ]
        ]
    )
);

```

Use for examples

```sh
/* ------------------------------ examples ------------------------------ */

// define extract class
$findNested = new ExtractNestedArray();

// you search field
$field = 'first_name';

// call execute for extract from data
$result = $findNested->extractNested($records, $field);


// get data : first_name
$field = 'first_name';
$result = $findNested->extractNested($records, $field);
echo sprintf('Get data for: %s -----> ', $field);
print_r($result);

$field = 'address';
$result = $findNested->extractNested($records, $field);
echo sprintf('Get data for: %s -----> ', $field);
print_r($result);

// get data : address -> city
$field = 'address.city';
$result = $findNested->extractNested($records, $field);
echo sprintf('Get data for: %s -----> ', $field);
print_r($result);

// get data : address -> city (use key: id)
$field = 'address.city';
$keyField = 'id';
$result = $findNested->extractNested($records, $field, $keyField);
echo sprintf('Get data for: %s -----> ', $field);
print_r($result);

$field = 'address.services';
$result = $findNested->extractNested($records, $field);
echo sprintf('Get data for: %s -----> ', $field);
print_r($result);

// get data : address -> services -> prop
$field = 'address.services.prop';
$result = $findNested->extractNested($records, $field);
echo sprintf('Get data for: %s -----> ', $field);
print_r($result);

// get data : address -> services -> prop -> all
$field = 'address.services.prop.*';
$result = $findNested->extractNested($records, $field);
echo sprintf('Get data for: %s -----> ', $field);
print_r($result);

// get data : address -> services -> prop -> 2 (get third item)
$field = 'address.services.prop.2';
$result = $findNested->extractNested($records, $field);
echo sprintf('Get data for: %s -----> ', $field);
print_r($result);

$field = 'cart.items';
$result = $findNested->extractNested($records, $field);
echo sprintf('Get data for: %s -----> ', $field);
print_r($result);

$field = 'cart.items.*';
$result = $findNested->extractNested($records, $field);
echo sprintf('Get data for: %s -----> ', $field);
print_r($result);

$field = 'cart.items.1';
$result = $findNested->extractNested($records, $field);
echo sprintf('Get data for: %s -----> ', $field);
print_r($result);

$field = 'cart.items.*.product.sku';
$result = $findNested->extractNested($records, $field);
echo sprintf('Get data for: %s -----> ', $field);
print_r($result);       

$field = 'cart.items.0.product.sku';
$result = $findNested->extractNested($records, $field);
echo sprintf('Get data for: %s -----> ', $field);
print_r($result);

$field = 'cart.items.1.product.sku';
$result = $findNested->extractNested($records, $field);
echo sprintf('Get data for: %s -----> ', $field);
print_r($result);

$field = 'cart.items.*.product';
$keyField = 'id';
$result = $findNested->extractNested($records, $field, $keyField);
echo sprintf('Get data for: %s -----> ', $field);
print_r($result);


```

Results
```sh
/* ------------------------------ results ------------------------------ */

Get data for: first_name -----> Array
(
    [0] => John
    [1] => Sally
    [2] => Jane
    [3] => Peter
)
Get data for: address -----> Array
(
    [0] => yok
    [1] => Array
        (
            [id] => 34
            [city] => ist
            [services] => Array
                (
                    [prop] => Array
                        (
                            [0] => fastDelivery
                            [1] => support
                            [2] => centerPoint
                        )

                )

        )

)
Get data for: address.city -----> Array
(
    [0] => ist
)
Get data for: address.city -----> Array
(
    [34] => ist
)
Get data for: address.services -----> Array
(
    [0] => Array
        (
            [prop] => Array
                (
                    [0] => fastDelivery
                    [1] => support
                    [2] => centerPoint
                )

        )

)
Get data for: address.services.prop -----> Array
(
    [0] => Array
        (
            [0] => fastDelivery
            [1] => support
            [2] => centerPoint
        )

)
Get data for: address.services.prop.* -----> Array
(
    [0] => fastDelivery
    [1] => support
    [2] => centerPoint
)
Get data for: address.services.prop.2 -----> Array
(
    [0] => centerPoint
)
Get data for: cart.items -----> Array
(
    [0] => Array
        (
            [0] => Array
                (
                    [id] => 232
                    [price] => 10
                    [quantity] => 1
                    [campaign] => 1
                    [product] => Array
                        (
                            [sku] => aa
                            [brand] => apple
                        )

                )

            [1] => Array
                (
                    [id] => 343
                    [price] => 20
                    [quantity] => 1
                    [product] => Array
                        (
                            [sku] => bb
                            [brand] => bosch
                            [category] => Array
                                (
                                    [0] => electronic
                                    [1] => phone
                                )

                        )

                )

        )

)
Get data for: cart.items.* -----> Array
(
    [0] => Array
        (
            [id] => 232
            [price] => 10
            [quantity] => 1
            [campaign] => 1
            [product] => Array
                (
                    [sku] => aa
                    [brand] => apple
                )

        )

    [1] => Array
        (
            [id] => 343
            [price] => 20
            [quantity] => 1
            [product] => Array
                (
                    [sku] => bb
                    [brand] => bosch
                    [category] => Array
                        (
                            [0] => electronic
                            [1] => phone
                        )

                )

        )

)
Get data for: cart.items.1 -----> Array
(
    [0] => Array
        (
            [id] => 343
            [price] => 20
            [quantity] => 1
            [product] => Array
                (
                    [sku] => bb
                    [brand] => bosch
                    [category] => Array
                        (
                            [0] => electronic
                            [1] => phone
                        )

                )

        )

)
Get data for: cart.items.*.product.sku -----> Array
(
    [0] => aa
    [1] => bb
)
Get data for: cart.items.0.product.sku -----> Array
(
    [0] => aa
)
Get data for: cart.items.1.product.sku -----> Array
(
    [0] => bb
)
Get data for: cart.items.*.product -----> Array
(
    [232] => Array
        (
            [sku] => aa
            [brand] => apple
        )

    [343] => Array
        (
            [sku] => bb
            [brand] => bosch
            [category] => Array
                (
                    [0] => electronic
                    [1] => phone
                )

        )

)
```

# License
MIT

# Contact
Questions, suggestions, comments:

Tahsin Yüksel info@tahsinyuksel.com
