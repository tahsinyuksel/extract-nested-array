<?php

namespace TyExtractNestedArray\Examples;

use TyExtractNestedArray\ExtractNestedArray;

class Example
{
	/**
	* execute examples
	*/
	public function execute()
	{
		// use data array
		$records = $this->loadRecords();

		// define extract class
		$findNested = new ExtractNestedArray();

		// you search field
		$field = 'first_name';

		// call execute for extract from data
		$result = $findNested->extractNested($records, $field);

		/* ------------------------------ examples ------------------------------ */

		// get data : first_name
		$field = 'first_name';
		$result = $findNested->extractNested($records, $field);
		echo sprintf('Get data for: %s -----> ', $field);
		print_r($result);

		$field = 'address';
		$result = $findNested->extractNested($records, $field);
		echo sprintf('Get data for: %s -----> ', $field);
		print_r($result);

		// get data : address -> city
		$field = 'address.city';
		$result = $findNested->extractNested($records, $field);
		echo sprintf('Get data for: %s -----> ', $field);
		print_r($result);

		// get data : address -> city (use key: id)
		$field = 'address.city';
		$keyField = 'id';
		$result = $findNested->extractNested($records, $field, $keyField);
		echo sprintf('Get data for: %s -----> ', $field);
		print_r($result);

		$field = 'address.services';
		$result = $findNested->extractNested($records, $field);
		echo sprintf('Get data for: %s -----> ', $field);
		print_r($result);

		// get data : address -> services -> prop
		$field = 'address.services.prop';
		$result = $findNested->extractNested($records, $field);
		echo sprintf('Get data for: %s -----> ', $field);
		print_r($result);

		// get data : address -> services -> prop -> all
		$field = 'address.services.prop.*';
		$result = $findNested->extractNested($records, $field);
		echo sprintf('Get data for: %s -----> ', $field);
		print_r($result);

		// get data : address -> services -> prop -> 2 (get third item)
		$field = 'address.services.prop.2';
		$result = $findNested->extractNested($records, $field);
		echo sprintf('Get data for: %s -----> ', $field);
		print_r($result);

		$field = 'cart.items';
		$result = $findNested->extractNested($records, $field);
		echo sprintf('Get data for: %s -----> ', $field);
		print_r($result);

		$field = 'cart.items.*';
		$result = $findNested->extractNested($records, $field);
		echo sprintf('Get data for: %s -----> ', $field);
		print_r($result);

		$field = 'cart.items.1';
		$result = $findNested->extractNested($records, $field);
		echo sprintf('Get data for: %s -----> ', $field);
		print_r($result);

		$field = 'cart.items.*.product.sku';
		$result = $findNested->extractNested($records, $field);
		echo sprintf('Get data for: %s -----> ', $field);
		print_r($result);		

		$field = 'cart.items.0.product.sku';
		$result = $findNested->extractNested($records, $field);
		echo sprintf('Get data for: %s -----> ', $field);
		print_r($result);

		$field = 'cart.items.1.product.sku';
		$result = $findNested->extractNested($records, $field);
		echo sprintf('Get data for: %s -----> ', $field);
		print_r($result);

		$field = 'cart.items.*.product';
		$keyField = 'id';
		$result = $findNested->extractNested($records, $field, $keyField);
		echo sprintf('Get data for: %s -----> ', $field);
		print_r($result);
	}

	/**
	* Get default data
	* @return array
	*/
	public function loadRecords()
	{
		$this->records = array(
		    array(
		        'id' => 2135,
		        'first_name' => 'John',
		        'last_name' => 'Doe',
		    ),
		    array(
		        'id' => 3245,
		        'first_name' => 'Sally',
		        'last_name' => 'Smith',
		    ),
		    array(
		        'id' => 5342,
		        'first_name' => 'Jane',
		        'last_name' => 'Jones',
		        'address' => 'yok',
		    ),
		    array(
		        'id' => 5623,
		        'first_name' => 'Peter',
		        'last_name' => 'Doe',
		        'age' => 2,
		        'address' => [
		            'id'=> 34,
		            'city'=> 'ist',
		            'services'=> [
		                'prop'=> [
		                    'fastDelivery',
		                    'support',
		                    'centerPoint'
		                ],
		            ],
		        ],
		        'cart'=> [
		            'payment'=> 'creditCard',
		            'total'=> 14.90,
		            'items'=> [
		                [
		                    'id'=> 232,
		                    'price'=> 10.00,
		                    'quantity'=> 1,
		                    'campaign'=> 1,
		                    'product'=> [
		                        'sku'=> 'aa',
		                        'brand'=> 'apple'
		                    ]
		                ],
		                [
		                    'id'=> 343,
		                    'price'=> 20.00,
		                    'quantity'=> 1,
		                    'product'=> [
		                        'sku'=> 'bb',
		                        'brand'=> 'bosch',
		                        'category'=> [
		                            'electronic',
		                            'phone'
		                        ]
		                    ]
		                ]
		            ]
		        ]
		    )
		);

		return $this->records;
	}
}