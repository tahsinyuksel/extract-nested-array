<?php

namespace TyExtractNestedArray;

class ExtractNestedArray 
{    
    /**
    * Extract my fields from data
    *
    * @param array|object $data
    * @param string $field
    * @param string|null index
    * @return array
    */
    public function extractNested($data, $field, $index = null) {

        foreach(explode('.', $field) as $segment) {

            if($segment == '*') {
                if(count($data) > 0) {
                    $data = ($data[0]);    
                }
                continue;
            }

            if(is_numeric($segment)) {
                $segment = intval($segment);
            }            

            $data = $this->extractData($data, $segment, $index);
        }

        return $data;
    }
    
    /**
    * Return the values from a single column in the input array
    *
    * @param array|object $data
    * @param string $field
    * @param string|null index
    * @return array
    */
    public function extractData($data, $field, $index = null){
        return array_column($data, $field, $index);
    }    
}